# To do list #
```
[x] BDD
[x] Accueil
	[x] Liste des packages (améliorable)
	[X] Recherche
	[x] Suppression de package
	[x] Edition de package (et de carte ?)
[x] Ajout de package
	[x] Informations packages (nom, tags)
	[x] Ajout de cartes
	[x] Listing de cartes
	[x] Import
[x] Session de jeu
	[x] Page d'introduction
	[x] Enchaînement de cartes
	[x] Feedback joueur
	[x] Timer
	[x] Page intermédiaire entre chaque deck
	[x] Page de résultat (rejouer ou retourner à l'accueil)
[x] Paramètres
	[x] About
	[x] Temps de réaction
[ ] Partage
	[ ] Twitter
	[ ] Facebook
```
