//
//  Package.m
//  iCards
//
//  Created by Guest User on 02/01/16.
//  Copyright (c) 2016 OriginalSin. All rights reserved.
//

#import "Package.h"
#import "Card.h"
#import "Tag.h"


@implementation Package

@dynamic name;
@dynamic tags;
@dynamic cards;

@end
