//
//  SettingsViewController.m
//  iCards
//
//  Created by Guest User on 02/01/16.
//  Copyright (c) 2016 OriginalSin. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@property (weak, nonatomic) IBOutlet UILabel *reactionTimeLabel;
@property (weak, nonatomic) IBOutlet UIStepper *reactionTimeStepper;

@end

@implementation SettingsViewController

@synthesize context;

- (IBAction)reactionTimeValueChanged:(UIStepper *)sender {
    double value = [sender value];
    
    [self.reactionTimeLabel setText:[NSString stringWithFormat:@"%d", (int)value]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSInteger reactionTime = [[NSUserDefaults standardUserDefaults] integerForKey:@"ReactionTime"];
    if (reactionTime == 0) {
        reactionTime = 10;
    }
    [self.reactionTimeStepper setValue:(double)reactionTime];
    [self.reactionTimeLabel setText:[NSString stringWithFormat:@"%d", (int)reactionTime]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Table view data source

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Help
    if (indexPath.section == 1 && indexPath.row == 1) {
        NSString *link = [[NSString alloc] initWithFormat:@"https://www.google.com"];
        NSURL *url = [NSURL URLWithString:link];
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    // Save settings
    [[NSUserDefaults standardUserDefaults] setInteger:self.reactionTimeStepper.value forKey:@"ReactionTime"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
