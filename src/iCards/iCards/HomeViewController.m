//
//  HomeViewController.m
//  iCards
//
//  Created by Jimmy Lanclume on 02/01/16.
//  Copyright © 2016 OriginalSin. All rights reserved.
//

#import "HomeViewController.h"
#import "AppDelegate.h"
#import "Package.h"
#import "Tag.h"
#import "AddPackageViewController.h"
#import "PackageViewController.h"

@interface HomeViewController ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property BOOL isFiltered;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) NSMutableArray *searchResultsArray;

-(BOOL)lookTextInTags:(NSSet*) tags textToLook:(NSString*)textToLook;

@end


@implementation HomeViewController

@synthesize context;
@synthesize packagesArray;
@synthesize searchResultsArray;
@synthesize searchBar;
@synthesize isFiltered;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    AppDelegate *delegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    self.context = delegate.managedObjectContext;
    
    // Set teh searchbar delegate as self
    searchBar.delegate = (id)self;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    if (isFiltered)
        return [searchResultsArray count];
    return [packagesArray count];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete object from DB
        Package *pack = [packagesArray objectAtIndex:indexPath.row];
        for (Card *card in pack.cards) {
            [context deleteObject:(NSManagedObject*)card];
        }
        [context deleteObject:(NSManagedObject*)pack];
        
        // Delete from array and table
        [packagesArray removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:YES];
        
        // Commit
        NSError *error = nil;
        if (![context save:&error]) {
            // Handle error
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Package";
    
    // Dequeue or create a new cell.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:CellIdentifier];
    }
    
    // Get row item, display its name and a checkmark if it was marked as completed.
    Package *package = NULL;
    if (isFiltered) {
        package = (Package *)[searchResultsArray objectAtIndex:indexPath.row];
    } else {
        package = (Package *)[packagesArray objectAtIndex:indexPath.row];
    }
    
    cell.textLabel.text = [package name];
    
    return cell;
}

#pragma mark - Search
-(void)searchBar:(UISearchBar*)searchBar textDidChange:(NSString*)text {
    // Callback call each time the text changed on the search bar
    
    if(text.length == 0)
    {
        isFiltered = NO;
    }
    else
    {
        isFiltered = YES;
        
        // Init the serach result array
        searchResultsArray = [[NSMutableArray alloc] init];
        
        // Use lowercase string for comparison
        text = [text lowercaseString];
        
        // Split the query
        NSArray* textsToLook = [text componentsSeparatedByString:@" "];
        
        // Iterate through the whole package array to find matches
        for (Package* pack in packagesArray)
        {
            // Lower case, for comparison
            NSString* name = [pack.name lowercaseString];
            BOOL packFinded = YES;
            
            for (NSString* textToLook in textsToLook) {
                if ([textToLook length] > 0) {
                    if (![name containsString:textToLook] && ![self lookTextInTags: pack.tags textToLook:textToLook]) {
                        packFinded = NO;
                        break;
                    }
                }
            }
            
            // If we find a match, we're good.
            if (packFinded) {
                [searchResultsArray addObject:pack];
            }
            
        }
    }
    [self.tableView reloadData];
}

-(BOOL)lookTextInTags:(NSSet*) tags textToLook:(NSString*)textToLook {
    // Try to find if a tag matches the query
    for (Tag* tag in tags) {
        if ([[tag.tag lowercaseString] containsString:textToLook]) {
            return YES;
        }
    }
    return NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    // Initialize itemsArray.
    packagesArray = [NSMutableArray new];
    
    // Request list of events entities.
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Package" inManagedObjectContext:context];
    [request setEntity:entity];
    
    // Sort items.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"name" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    [request setSortDescriptors:sortDescriptors];
    
    // Fetch mutable array of results.
    NSError *error = nil;
    NSMutableArray *mutableFetchResults = [[context executeFetchRequest:request error:&error] mutableCopy];
    if (mutableFetchResults == nil) {
        // Handle the error.
    }
    
    // Initialize items array with fetched results.
    [self setPackagesArray:mutableFetchResults];

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (sender == self.navigationItem.rightBarButtonItem || sender == self.navigationItem.leftBarButtonItem) {
        AddPackageViewController *controller = (AddPackageViewController *)((UINavigationController *)[segue destinationViewController]).topViewController;
        controller.context = context;
    }
    else {
        PackageViewController *controller = (PackageViewController *)((UINavigationController *)[segue destinationViewController]).topViewController;
        controller.context = context;
        for (Package *package in packagesArray) {
            if (package.name == ((UITableViewCell *) sender).textLabel.text) {
                controller.package = package;
                break;
            }
        }
        
    }
}

@end
