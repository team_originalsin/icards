//
//  AddPackageViewController.m
//  iCards
//
//  Created by Guest User on 02/01/16.
//  Copyright (c) 2016 OriginalSin. All rights reserved.
//

#import "EditPackageViewController.h"
#import "EditCardViewController.h"
#import "Tag.h"
#import "Card.h"

@interface EditPackageViewController ()

@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UITextField *packageNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *packageTagsTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *importButton;

@property (weak, nonatomic) IBOutlet UITextField *NameTextField;
@property (weak, nonatomic) IBOutlet UITextField *TagsTextField;
@property NSString *originalName;

@end

@implementation EditPackageViewController

@synthesize context;
@synthesize nextButton;
@synthesize packageNameTextField;
@synthesize packageTagsTextField;
@synthesize package;
@synthesize importButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.NameTextField.text = package.name;
    NSMutableArray* tags = [[self.package.tags allObjects] mutableCopy];
    NSMutableArray *newArray = [NSMutableArray new];
    for (Tag *tag in tags) {
        [newArray addObject:[NSString stringWithFormat:@"%@", [tag tag]]];
    }
    self.TagsTextField.text = [newArray componentsJoinedByString:@" "];
    
    self.originalName = package.name;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (Package *)editPackage:(NSString *)name tags:(NSArray *)tags error:(NSString**)error {
    // Package init
    package.name = name;
    [package removeTags:package.tags];
    
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSError *executeFetchError = nil;
    // Create package tags set
    NSMutableSet *tagsSet = [NSMutableSet new];
    
    for (NSString *s in tags) {
        if ([s length] > 0) {
            Tag *tag = NULL;
            request = [[NSFetchRequest alloc] init];
            request.entity = [NSEntityDescription entityForName:@"Tag" inManagedObjectContext:context];
            request.predicate = [NSPredicate predicateWithFormat:@"tag = %@", s];
            
            tag = [[context executeFetchRequest:request error:&executeFetchError] lastObject];
            if (tag == NULL) {
                tag = (Tag *)[NSEntityDescription insertNewObjectForEntityForName:@"Tag" inManagedObjectContext:context];
                [tag setTag:s];
            }
            
            [tag addPackagesObject:package];
            [tagsSet addObject:tag];
        }
    }
    [package setTags:tagsSet];
    return package;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if (sender == nextButton) {
        NSString* error = nil;
        [self editPackage:packageNameTextField.text
                    tags:[packageTagsTextField.text componentsSeparatedByString:@" "]
                   error:&error];
        
        if ([error length] > 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:error
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            return NO;
        }
    }
    return YES;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (sender == nextButton) {
        // Pass the context and the package
        EditCardViewController *controller = (EditCardViewController *)((UINavigationController *)[segue destinationViewController]).topViewController;
        controller.context = context;
        controller.package = package;
    } else {
        // Other cases means that we must rollback
        [context rollback];
    }
}
- (IBAction)textChanged:(UITextField *)sender {
    [nextButton setEnabled:[packageNameTextField.text length] != 0];
}


@end
