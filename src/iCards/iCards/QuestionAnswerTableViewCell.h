//
//  QuestionAnswerTableViewCell.h
//  iCards
//
//  Created by Jimmy Lanclume on 03/01/16.
//  Copyright © 2016 OriginalSin. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface QuestionAnswerTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@property (weak, nonatomic) IBOutlet UILabel *answerLabel;

@end
