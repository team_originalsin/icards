//
//  SessionViewController.h
//  iCards
//
//  Created by Guest User on 02/01/16.
//  Copyright (c) 2016 OriginalSin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Package.h"

@interface SessionViewController : UIViewController<UIAlertViewDelegate>

@property (strong, nonatomic) NSManagedObjectContext *context;
@property (weak, nonatomic) Package *package;
@property (strong, nonatomic) NSMutableArray* successDeck;

@end
