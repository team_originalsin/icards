//
//  ListCardTableViewController.h
//  iCards
//
//  Created by Guest User on 03/01/16.
//  Copyright (c) 2016 OriginalSin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Package.h"

@interface ListCardViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) NSManagedObjectContext *context;
@property (weak, nonatomic) Package *package;

@end
