//
//  EditCardViewController.h
//  iCards
//
//  Created by Guest User on 07/01/16.
//  Copyright (c) 2016 OriginalSin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Card.h"
#import "Package.h"

@interface EditCardViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray *cardsArray;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) Package *package;

- (void)clearQuestions;

@end
