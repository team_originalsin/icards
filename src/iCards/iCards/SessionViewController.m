//
//  SessionViewController.m
//  iCards
//
//  Created by Guest User on 02/01/16.
//  Copyright (c) 2016 OriginalSin. All rights reserved.
//

#import "SessionViewController.h"
#import "ResultViewController.h"
#import "Card.h"

@interface SessionViewController ()

@property (weak, nonatomic) IBOutlet UIProgressView *progress;
@property (weak, nonatomic) IBOutlet UIProgressView *timerView;
@property (weak, nonatomic) IBOutlet UIButton *failButton;
@property (weak, nonatomic) IBOutlet UIButton *successButton;
@property (weak, nonatomic) IBOutlet UIButton *cardText;
@property (weak, nonatomic) IBOutlet UIView *cardView;

@property (strong, nonatomic) NSMutableArray* nextDeck;
@property (strong, nonatomic) NSMutableArray* currentDeck;
@property (nonatomic) NSInteger initialCountDeck;
@property (strong, nonatomic) Card* card;
@property (nonatomic) BOOL hiddenCard;
@property (nonatomic) NSTimer* timer;
@property (nonatomic) float barStep;

- (void) sessionLoop;
- (void) initializeGuess;
- (BOOL) setupNextDeck;
- (void) endSession;
- (void) increaseProgressValue;
- (IBAction)toggleCard;
- (void) showIntermediateDialog;

@end

@implementation SessionViewController

@synthesize progress;
@synthesize currentDeck;
@synthesize nextDeck;
@synthesize successDeck;
@synthesize card;
@synthesize hiddenCard;
@synthesize initialCountDeck;
@synthesize cardView;
@synthesize timerView;
@synthesize timer;
@synthesize barStep;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Initialize current deck with package's cards.
    currentDeck = [[self.package.cards allObjects] mutableCopy];
    nextDeck = [NSMutableArray new];
    successDeck = [NSMutableArray new];
    hiddenCard = FALSE;
    initialCountDeck = [currentDeck count];
    
    // Update all cards of current deck.
    for (Card *cardDeck in self.package.cards) {
        [cardDeck setNb_try: [NSNumber numberWithInteger: [cardDeck.nb_try integerValue] + 1]];
    }
    
    NSError* error;
    if ([self.context save:&error]) {
        // Handle error.
    }
    
    // Launch session.
    progress.progress = 0.0f;
    [self sessionLoop];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) sessionLoop {
    // If there is no card left in the current deck, try to set it up with the next deck.
    if ([self.currentDeck count] == 0) {
        // Show intermediate dialog if the next deck is not empty.
        if ([nextDeck count] > 0) {
            [self showIntermediateDialog];
            return;
        }
        // If the next deck is empty, end session.
        if (![self setupNextDeck]) {
            [self endSession];
            return;
        }
    }
    
    // Otherwise, set current card appropriately.
    [self initializeGuess];
}

- (void) initializeGuess {
    self.card = currentDeck[0];
    [currentDeck removeObjectAtIndex:0];
    [self toggleCard];
    self.timerView.progress = 0.0f;
    self.timer = [self createTimer];
    self.successButton.enabled = TRUE;
}


- (BOOL) setupNextDeck {
    // If the next deck has cards in it, transfer those into the current deck.
    if ([nextDeck count] > 0) {
        // Update all cards of current deck.
        currentDeck = nextDeck;
        initialCountDeck = [currentDeck count];
        nextDeck = [NSMutableArray new];
        progress.progress = 0.0f;
        
        // Update all cards of current deck.
        for (Card *cardDeck in self.currentDeck) {
            [cardDeck setNb_try: [NSNumber numberWithInteger: [cardDeck.nb_try integerValue] + 1]];
        }
        
        NSError* error;
        if ([self.context save:&error]) {
            // Handle error.
        }
        
        return TRUE;
    }
    
    return FALSE;
}

- (IBAction)toggleCard {
    // If the card was not hidden, toggle to hidden state.
    if (!hiddenCard) {
        self.failButton.hidden = YES;
        self.successButton.hidden = YES;
        self.cardText.enabled = TRUE;
        [self.cardText setTitle:card.recto forState: UIControlStateNormal];
    }
    else { // Otherwise, show it.
        self.failButton.hidden = NO;
        self.successButton.hidden = NO;
        self.cardText.enabled = FALSE;
        [self.cardText setTitle:card.verso forState: UIControlStateNormal];
        [self increaseProgressValue];
        [self.timer invalidate];
        self.timer = nil;
    }
    
    // Reverse state.
    self.hiddenCard = !self.hiddenCard;
    
    self.cardText.hidden = TRUE;
    
    [UIView transitionWithView:cardView
                      duration:0.5
                       options:UIViewAnimationOptionTransitionFlipFromLeft
                    animations:nil
                    completion:^(BOOL finished) {
                        self.cardText.hidden = FALSE;
                    }
     ];
}

- (IBAction)handleFeedback:(id)sender {
    // If the feedback is positive, increase nb_success.
    if (sender == self.successButton) {
        [self.successDeck addObject:self.card];
    }
    else {
        [nextDeck addObject:card];
    }
    
    // Change card if any.
    [self sessionLoop];
}

- (void) increaseProgressValue {
    // Increase progress view's value.
    progress.progress = progress.progress + (float) (100 / [self initialCountDeck]) / 100;
    if ([currentDeck count] == 0)
        progress.progress = 1.0f;
}

- (void) endSession {
    [self performSegueWithIdentifier:@"GO_TO_RESULT" sender:self.cardView];
}

- (void)timerTicked:(NSTimer*)timer {
    timerView.progress = timerView.progress + self.barStep;
    
    // Change color of progress bar
    if (timerView.progress == 1.0f) {
        // If it is complete before the user tapped the card, disable the positive button and toggle card.
        timerView.progressTintColor = [UIColor redColor];
        self.successButton.enabled = FALSE;
        if (hiddenCard) {
            [self toggleCard];
        }
    }
    else if (timerView.progress > 0.5f){
        timerView.progressTintColor = [UIColor orangeColor];
    }
    else {
        timerView.progressTintColor = [UIColor greenColor];
    }
}

- (NSTimer*)createTimer {
    NSInteger reactionTime = [[NSUserDefaults standardUserDefaults] integerForKey:@"ReactionTime"];
    if (reactionTime == 0) {
        reactionTime = 10;
    }
    self.barStep = 1.0f / ((float)reactionTime * 10.0f);
    
    timerView.progress = 0.0f;
    return [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timerTicked:) userInfo:nil repeats:YES];
}

- (void) showIntermediateDialog {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Ready for the next deck?"
                                                    message:@"Your training was not perfect, we will now focus on the failed cards."
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        [self setupNextDeck];
        [self initializeGuess];
    }
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (sender == self.cardView) {
        ResultViewController *controller = (ResultViewController *)((UINavigationController *)[segue destinationViewController]);
        controller.context = self.context;
        controller.package = self.package;
    }
}


@end
    
    
    
