//
//  Card.h
//  iCards
//
//  Created by Guest User on 02/01/16.
//  Copyright (c) 2016 OriginalSin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Package;

@interface Card : NSManagedObject

@property (nonatomic, retain) NSString * recto;
@property (nonatomic, retain) NSString * verso;
@property (nonatomic, retain) NSNumber * nb_try;
@property (nonatomic, retain) NSNumber * nb_success;
@property (nonatomic, retain) Package *package;

@end
