//
//  PackageViewController.m
//  iCards
//
//  Created by Guest User on 02/01/16.
//  Copyright (c) 2016 OriginalSin. All rights reserved.
//

#import "PackageViewController.h"
#import "SessionViewController.h"
#import "ListCardViewController.h"
#import "EditPackageViewController.h"
#import "Card.h"
#import "Package.h"
#import "Tag.h"

@interface PackageViewController ()

@property (weak, nonatomic) IBOutlet UILabel *PackageNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *statsLabel;
@property (weak, nonatomic) IBOutlet UILabel *tagsLabel;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIButton *cardsButton;

@end

@implementation PackageViewController

@synthesize context;
@synthesize package;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.nameLabel.text = package.name;
    int success = 0;
    int total = 0;
    for (Card* card in package.cards) {
        total += [card.nb_try intValue];
        success += [card.nb_success intValue];
    }
    self.statsLabel.text = [NSString stringWithFormat:@"%d/%d", success, total];
    
    NSMutableArray* tags = [[self.package.tags allObjects] mutableCopy];
    NSMutableArray *newArray = [NSMutableArray new];
    for (Tag *tag in tags) {
        [newArray addObject:[NSString stringWithFormat:@"#%@", [tag tag]]];
    }
    self.tagsLabel.text = [newArray componentsJoinedByString:@" "];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (sender == self.playButton) {
        SessionViewController *controller = (SessionViewController *)((UINavigationController *)[segue destinationViewController]).topViewController;
        controller.context = context;
        controller.package = package;
    }
    else if (sender == self.cardsButton) {
        ListCardViewController *controller = (ListCardViewController *)((UINavigationController *)[segue destinationViewController]).topViewController;
        controller.context = context;
        controller.package = package;
    } else {
        EditPackageViewController *controller = (EditPackageViewController *)((UINavigationController *)[segue destinationViewController]).topViewController;
        controller.context = context;
        controller.package = package;
    }
}


@end
