//
//  Card.m
//  iCards
//
//  Created by Guest User on 02/01/16.
//  Copyright (c) 2016 OriginalSin. All rights reserved.
//

#import "Card.h"
#import "Package.h"


@implementation Card

@dynamic recto;
@dynamic verso;
@dynamic nb_try;
@dynamic nb_success;
@dynamic package;

@end
