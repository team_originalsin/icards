//
//  ReplaceTopSegue.m
//  iCards
//
//  Created by Guest User on 03/01/16.
//  Copyright (c) 2016 OriginalSin. All rights reserved.
//

#import "ReplaceTopSegue.h"


@implementation ReplaceTopSegue
- (void)perform {
    UIViewController *sourceViewController = (UIViewController*)[self sourceViewController];
    UIViewController *destinationController = (UIViewController*)[self destinationViewController];
    UINavigationController *navigationController = sourceViewController.navigationController;
    
    // Get a changeable copy of the stack
    NSMutableArray *controllerStack = [NSMutableArray arrayWithArray:navigationController.viewControllers];
    // Replace the source controller with the destination controller, wherever the source may be
    [controllerStack replaceObjectAtIndex:[controllerStack indexOfObject:sourceViewController] withObject:destinationController];
    
    // Assign the updated stack with animation
    [navigationController setViewControllers:controllerStack animated:NO];
}
@end
