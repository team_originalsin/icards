//
//  SettingsViewController.h
//  iCards
//
//  Created by Guest User on 02/01/16.
//  Copyright (c) 2016 OriginalSin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UITableViewController

@property (strong, nonatomic) NSManagedObjectContext *context;

- (IBAction)reactionTimeValueChanged:(UIStepper *)sender;

@end
