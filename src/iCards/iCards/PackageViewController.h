//
//  PackageViewController.h
//  iCards
//
//  Created by Guest User on 02/01/16.
//  Copyright (c) 2016 OriginalSin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Package.h"

@interface PackageViewController : UIViewController

@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) Package *package;

@end
