//
//  AddPackageViewController.m
//  iCards
//
//  Created by Guest User on 02/01/16.
//  Copyright (c) 2016 OriginalSin. All rights reserved.
//

#import "AddPackageViewController.h"
#import "AddCardViewController.h"
#import "Package.h"
#import "Tag.h"
#import "Card.h"

@interface AddPackageViewController ()

@property (weak, nonatomic) IBOutlet UIButton *nextButton;
@property (weak, nonatomic) IBOutlet UITextField *packageNameTextField;
@property (weak, nonatomic) IBOutlet UITextField *packageTagsTextField;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *importButton;
@property Package* package;

- (void)failImport:(NSString *)message;
- (void)finishImport;

@end

@implementation AddPackageViewController

@synthesize context;
@synthesize nextButton;
@synthesize packageNameTextField;
@synthesize packageTagsTextField;
@synthesize package;
@synthesize importButton;

- (void)viewDidLoad {
    [super viewDidLoad];
    [nextButton setEnabled:NO];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)importButtonClick:(id)sender {
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Import new package" message:@"Please enter the URL to the package:" delegate:self cancelButtonTitle:@"Import" otherButtonTitles:nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    UITextField * alertTextField = [alert textFieldAtIndex:0];
    alertTextField.keyboardType = UIKeyboardTypeURL;
    alertTextField.text = @"http://pastebin.com/raw/395CcV74";
    [alert show];
}



- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    // Black overlay
    UIView *overlayView = [[UIView alloc] initWithFrame:self.navigationController.view.frame];
    overlayView.backgroundColor = [UIColor blackColor];
    overlayView.alpha = 0.8;
    overlayView.tag = 88;
    
    // Message
    UILabel *message = [[UILabel alloc] initWithFrame:self.navigationController.view.frame];
    [message setFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:25.0f]];
    message.text = @"Your package is being imported";
    message.textColor = [UIColor whiteColor];
    message.textAlignment = NSTextAlignmentCenter;
    message.tag = 99;
    
    // Spinner
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.center = CGPointMake(self.navigationController.view.frame.size.width / 2.0, self.navigationController.view.frame.size.height / 2.0 + 40);
    activityIndicator.tag = 98;
    [activityIndicator startAnimating];
    
    // Add views
    [self.navigationController.view addSubview:overlayView];
    [self.navigationController.view addSubview:message];
    [self.navigationController.view addSubview:activityIndicator];
    
    // Download file
    NSString *urlAsString = [[alertView textFieldAtIndex:0] text];
    NSURL *url = [[NSURL alloc] initWithString:urlAsString];
    [NSURLConnection sendAsynchronousRequest:[[NSURLRequest alloc] initWithURL:url] queue:[[NSOperationQueue alloc] init] completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
        if (error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [self failImport:error.localizedDescription];
            });
        } else {
            NSLog(@"%@", urlAsString);
            
            // Parse JSON
            NSError *error = nil;
            NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
            
            // JSON error
            if (error != nil) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self failImport:error.localizedDescription];
                });
            } else {
                // Save package
                NSString* packageError = nil;
                Package *pack = [self addPackage:[parsedObject objectForKey:@"name"]
                                            tags:[parsedObject valueForKey:@"tags"]
                                           error:&packageError];
                if ([packageError length] > 0) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self failImport:packageError];
                    });
                } else {
                    
                    // Save cards
                    NSArray *cards = [parsedObject valueForKey:@"cards"];
                    for (NSDictionary *jcard in cards) {
                        Card* card = [NSEntityDescription insertNewObjectForEntityForName:@"Card" inManagedObjectContext:context];
                        card.package = pack;
                        [card setRecto:[jcard valueForKey:@"question"]];
                        [card setVerso:[jcard valueForKey:@"answer"]];
                    }
                    
                    // Save data and handle errors
                    NSError* saveError = nil;
                    if (![context save:&saveError]) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self failImport:saveError.localizedDescription];
                        });
                    }
                    else {
                        // Finish import
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self finishImport];
                        });
                    }
                }
            }
        }
    }];
}

- (void)failImport:(NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Import new package"
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
    
    // Remove overlays
    UIView *view = (UIView *)[self.navigationController.view viewWithTag:88];
    UILabel *label = (UILabel *)[self.navigationController.view viewWithTag:99];
    UIActivityIndicatorView *spinner = (UIActivityIndicatorView *)[self.navigationController.view viewWithTag:98];
    [UIView animateWithDuration:0.5
                     animations:^{
                         view.alpha = 0.0;
                         label.alpha = 0.0;
                         spinner.alpha = 0.0;
                     }
                     completion:^(BOOL finished){
                         [view removeFromSuperview];
                         [label removeFromSuperview];
                         [spinner removeFromSuperview];
                     }
     ];
}

- (void)finishImport {
    // Remove overlays
    UIView *view = (UIView *)[self.navigationController.view viewWithTag:88];
    UILabel *label = (UILabel *)[self.navigationController.view viewWithTag:99];
    UIActivityIndicatorView *spinner = (UIActivityIndicatorView *)[self.navigationController.view viewWithTag:98];
    [view removeFromSuperview];
    [label removeFromSuperview];
    [spinner removeFromSuperview];
    
    // Popup :)
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Succes"
                                                    message:@"Import done succesfully!"
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (Package *)addPackage:(NSString *)name tags:(NSArray *)tags error:(NSString**)error {
    // Package init
    package = NULL;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Package" inManagedObjectContext:context];
    request.predicate = [NSPredicate predicateWithFormat:@"name = %@", name];
    NSError *executeFetchError = nil;
    package = [[context executeFetchRequest:request error:&executeFetchError] lastObject];
    
    if (executeFetchError) {
        *error = @"An unexpected error occured";
        return nil;
    } else if (package != NULL) {
        *error = [NSString stringWithFormat:@"A package with the same name (%@) already exist", name];
        return nil;
    }
    
    package = (Package *)[NSEntityDescription insertNewObjectForEntityForName:@"Package" inManagedObjectContext:context];
    [package setName:name];
    
    // Create package tags set
    NSMutableSet *tagsSet = [NSMutableSet new];
    
    for (NSString* s in tags) {
        if ([s length] > 0) {
            Tag *tag = NULL;
            request = [[NSFetchRequest alloc] init];
            request.entity = [NSEntityDescription entityForName:@"Tag" inManagedObjectContext:context];
            request.predicate = [NSPredicate predicateWithFormat:@"tag = %@", s];
            
            tag = [[context executeFetchRequest:request error:&executeFetchError] lastObject];
            if (tag == NULL) {
                tag = (Tag *)[NSEntityDescription insertNewObjectForEntityForName:@"Tag" inManagedObjectContext:context];
                [tag setTag:s];
            }
            
            [tag addPackagesObject:package];
            [tagsSet addObject:tag];
        }
    }
    
    [package setTags:tagsSet];
    
    return package;
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if (sender == nextButton) {
        NSString* error = nil;
        [self addPackage:packageNameTextField.text
                    tags:[packageTagsTextField.text componentsSeparatedByString:@" "]
                   error:&error];
        
        if ([error length] > 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                            message:error
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            return NO;
        }
    }
    return YES;
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (sender == nextButton) {
        // Pass the context and the package
        AddCardViewController *controller = (AddCardViewController *)((UINavigationController *)[segue destinationViewController]).topViewController;
        controller.context = context;
        controller.package = package;
    } else {
        // Other cases means that we must rollback
        [context rollback];
    }
}
- (IBAction)textChanged:(UITextField *)sender {
    [nextButton setEnabled:[packageNameTextField.text length] != 0];
}


@end
