//
//  AddCardViewController.h
//  iCards
//
//  Created by Jimmy Lanclume on 02/01/16.
//  Copyright © 2016 OriginalSin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Package.h"

@interface AddCardViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSMutableArray *cardsArray;
@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) Package *package;

- (void)clearQuestions;

@end

