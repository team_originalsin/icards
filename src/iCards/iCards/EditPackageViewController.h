//
//  EditPackageViewController.h
//  iCards
//
//  Created by Guest User on 07/01/16.
//  Copyright (c) 2016 OriginalSin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Package.h"

@interface EditPackageViewController : UIViewController

@property (strong, nonatomic) NSManagedObjectContext *context;
@property (strong, nonatomic) Package *package;

@end
