//
//  HomeViewController.h
//  iCards
//
//  Created by Jimmy Lanclume on 02/01/16.
//  Copyright © 2016 OriginalSin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSManagedObjectContext *context;
@property (nonatomic, strong) NSMutableArray *packagesArray;

@end