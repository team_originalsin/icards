//
//  QuestionAnswerTableViewCell.m
//  iCards
//
//  Created by Jimmy Lanclume on 03/01/16.
//  Copyright © 2016 OriginalSin. All rights reserved.
//

#import "QuestionAnswerTableViewCell.h"

@implementation QuestionAnswerTableViewCell

@synthesize questionLabel;
@synthesize answerLabel;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
