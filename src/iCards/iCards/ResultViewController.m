//
//  ResultViewController.m
//  iCards
//
//  Created by Guest User on 02/01/16.
//  Copyright (c) 2016 OriginalSin. All rights reserved.
//

#import "ResultViewController.h"
#import "SessionViewController.h"
#import "Card.h"

@interface ResultViewController ()
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIButton *againButton;

@end

@implementation ResultViewController

@synthesize package;
@synthesize context;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nameLabel.text = package.name;
    self.scoreLabel.text = [NSString stringWithFormat:@"Success !\nYou mastered %lu cards !", [package.cards count]];
    
    for (Card *card in package.cards) {
        [card setNb_success: [NSNumber numberWithInteger: [card.nb_success integerValue] + 1]];
    }
    
    // Save data and handle errors
    NSError* error = nil;
    if (![context save:&error]){
    // TODO: error handling
    }
}


- (IBAction)handleFeedback:(id)sender {
    [self tryAgain];
}

- (void) tryAgain {
    [self performSegueWithIdentifier:@"GO_TO_SESSION" sender:self.againButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (sender == self.againButton) {
        SessionViewController *controller = (SessionViewController *)((UINavigationController *)[segue destinationViewController]);
        controller.context = context;
        controller.package = package;
    }
}


@end
