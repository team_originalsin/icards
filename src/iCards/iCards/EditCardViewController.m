//
//  AddCardViewController.m
//  iCards
//
//  Created by Jimmy Lanclume on 02/01/16.
//  Copyright © 2016 OriginalSin. All rights reserved.
//

#import "EditCardViewController.h"
#import "Card.h"
#import "PackageViewController.h"
#import "QuestionAnswerTableViewCell.h"

@interface EditCardViewController ()

@property (weak, nonatomic) IBOutlet UITextField *questionTextField;
@property (weak, nonatomic) IBOutlet UITextField *answerTextField;
@property (weak, nonatomic) IBOutlet UITableView *questionsTableView;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *resetButton;

@end


@implementation EditCardViewController

@synthesize cardsArray;
@synthesize context;
@synthesize package;
@synthesize addButton;
@synthesize resetButton;
@synthesize questionsTableView;
@synthesize questionTextField;
@synthesize answerTextField;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.questionsTableView.delegate = self;
    self.questionsTableView.dataSource = self;
    self.cardsArray = [NSMutableArray new];
    [self.addButton setEnabled:NO];
    
    for (Card *card in package.cards) {
        NSArray* cardData = [[NSMutableArray alloc] initWithObjects:card.recto, card.verso, nil];
        [cardsArray addObject:cardData];
    }
    
    [self.resetButton setEnabled:([self.cardsArray count] != 0)];
    [self.navigationItem.rightBarButtonItem setEnabled:([self.cardsArray count] > 0)];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)textChanged:(UITextField *)sender {
    BOOL enableButtons = [questionTextField.text length] != 0 && [answerTextField.text length] != 0;
    [addButton setEnabled:enableButtons];
}

- (IBAction)addButtonPressed:(UIButton *)sender {
    Card* card = nil;
    
    // if the card already exist: fetch and display a popup
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    request.entity = [NSEntityDescription entityForName:@"Card" inManagedObjectContext:context];
    request.predicate = [NSPredicate predicateWithFormat:@"recto = %@", questionTextField.text];
    NSError *executeFetchError = nil;
    card = [[context executeFetchRequest:request error:&executeFetchError] lastObject];
    
    if (executeFetchError) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"An unexpected error occured"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    } else if (card != NULL) {
        Package* oldPackage = card.package;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:[NSString stringWithFormat:@"This card already exist in the '%@' package", oldPackage.name]
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    } else if ([cardsArray containsObject:[[NSMutableArray alloc] initWithObjects:questionTextField.text, answerTextField.text, nil]]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                        message:@"This card already exist in this package"
                                                       delegate:nil
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        
    } else {
        // add a new card
        NSArray* cardData = [[NSMutableArray alloc] initWithObjects:questionTextField.text, answerTextField.text, nil];
        
        [cardsArray addObject:cardData];
        
        // reload the list data
        [questionsTableView reloadData];
        
        // Enable (or not) buttons
        [self.resetButton setEnabled:YES];
        if ([cardsArray count] > 2) {
            [self.navigationItem.rightBarButtonItem setEnabled:YES];
        }
    }
}

- (IBAction)resetButtonPressed:(UIButton *)sender {
    [self clearQuestions];
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    [questionsTableView reloadData];
}

- (void)clearQuestions {
    [cardsArray removeAllObjects];
    [self.resetButton setEnabled:NO];
    [self.addButton setEnabled:NO];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [cardsArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Question";
    
    // Dequeue or create a new cell.
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:CellIdentifier];
    }
    
    // Get row item, display its name and a checkmark if it was marked as completed.
    
    NSArray *cardData = (NSArray *)[cardsArray objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@ / %@", cardData[0], cardData[1]];
    
    
    return cell;
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if (sender == self.navigationItem.rightBarButtonItem) {
        
        [package removeCards:package.cards];
        NSMutableSet *cardsSet = [NSMutableSet new];
        for (NSArray* cardStr in cardsArray) {
            Card* card = [NSEntityDescription insertNewObjectForEntityForName:@"Card" inManagedObjectContext:context];
            card.package = package;
            
            [card setRecto:cardStr[0]];
            [card setVerso:cardStr[1]];
            [cardsSet addObject:card];
        }
        [package setCards:cardsSet];
    
        // Save data and handle errors
        NSError* error = nil;
        if (![context save:&error]){
            // TODO: error handling
        }
        PackageViewController *controller = (PackageViewController *)((UINavigationController *)[segue destinationViewController]);
        controller.context = context;
        controller.package = package;        
    } else {
        [context rollback];
    }
}


@end
